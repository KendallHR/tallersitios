﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RetoGrupo7Pablo.aspx.cs" Inherits="GitLabRetos.Paginas.RetoGrupo7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div id="nombre" runat="server" style="background-color: mistyrose">
        </div>

        <div>
            <label>Nombre:</label><br />
            <asp:TextBox runat="server" ID="txtNombre" />
        </div>

        <div>
            <label>Comentario:</label><br />
            <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine"></asp:TextBox>

        </div>

        <div>
            <asp:Button ID="btnAgregar" runat="server" Text="Comentar"/>
        </div>
            
        <div>
            <asp:GridView ID="grdComentarios" runat="server" Width="684px"></asp:GridView>
        </div>
    </section>
</asp:Content>

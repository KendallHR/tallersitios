﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GitLabRetos.Paginas
{
    public partial class RetoGrupo5Kevin : System.Web.UI.Page
    {
        int id = 10;
        rotected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                BLL.estudiantes estudiante = new BLL.estudiantes();
                DataTable dtNombre = estudiante.consultarNombres();

                String nombre = "<h1 class="text-center">" + dtNombre.Rows[id - 1].ItemArray[0] + "</h1>";
                this.nombre.InnerHtml = nombre;

                traerComentarios();
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            BLL.estudiantes comentario = new BLL.estudiantes();
            comentario.ingresarComentarios(id, this.txtNombre.Text, this.txtComentario.Text);

            traerComentarios();

            txtNombre.Text = "";
            txtComentario.Text = "";
        }

        public void traerComentarios()
        {
            BLL.estudiantes comentario = new BLL.estudiantes();
            DataTable comentarios = comentario.consultarComentariosPorPersona(id);
            grdComentarios.DataSource = comentarios;
            grdComentarios.DataBind();
        }
    }
}
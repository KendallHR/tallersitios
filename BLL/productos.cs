﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class productos
    {
        Data.datos misDatos = new Data.datos();
        public DataTable consultarProductos()
        {
            try
            {
                misDatos.consutarProductos();
                return misDatos.Tabla_Datos;
            }
                catch (Exception ex){
                throw ex;
            }
        }

        public DataTable consultarMarcas()
        {
            try
            {
                misDatos.consutarMarcas();
                return misDatos.Tabla_Datos;
            }
                catch (Exception ex){
                throw ex;
            }
        }

        public DataTable buscarProductoPorID(int id)
        {
            try
            {
                misDatos.consutarProductoPorId(id);
                return misDatos.Tabla_Datos;
            }
                catch (Exception ex){
                throw ex;
            }
        }

        public DataTable buscarProductoPorIDMarca(int id)
        {
            try
            {
                misDatos.consutarProductoPorIdMarca(id);
                return misDatos.Tabla_Datos;
            }
                catch (Exception ex){
                throw ex;
            }
        }

        public void ingresarComentarios(int id, string nombre, string comentario)
        {
            try
            {
                misDatos.ingresarComentarioProducto(id,nombre,comentario);                
            }
                catch (Exception ex){
                throw ex;
            }
        }
        public DataTable buscarComentarios(int id)
        {
            try
            {
                misDatos.consultarComentariosPorProducto(id);
                return misDatos.Tabla_Datos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
